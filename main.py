#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import random
import sys
from automovil import Auto
import os


def gasto_ruedas(ruedas):
    # Se genera el gasto en las ruedas
    i = 0
    for i in range(len(ruedas)):
        ruedas[i] = ruedas[i] - random.randint(1, 11)
    return ruedas


def estado_ruedas(ruedas):
    # Mecanico es la condicion para que se arreglen las ruedas
    mecanico = 0
    i = 0
    for i in range(len(ruedas)):
        # Se imprimen las ruedas y su porcentaje
        print("Rueda", i, ":", ruedas[i], "%")
        if ruedas[i] < 10:
            mecanico += 1
    # Si una o mas ruedas tienen menos del 10 % se llama a los pits
    if mecanico > 0:
        a_los_pits(ruedas)


def a_los_pits(ruedas):
    i = 0
    # Se recorre la lista con las ruedas y si estan malas se reparan
    for i in range(len(ruedas)):
        if ruedas[i] < 10:
            print("La rueda", 1, "esta a un", ruedas[i], "de % su capacidad",
            "es necesario cabiarla")
            print("Rueda cambiada esta al 100 %")
            ruedas[i] = 100


def avanzar(automovil, energia, distancia, tiempo):
    # Se obtiene velocidad
    obtener_velocidad = automovil.velocidad()
    velocidad = automovil.get_velocidad()
    # Condicion si la velocidad es muy alta
    if velocidad > 120:
        print("quien te crees, Arturo Vidal")
        avanzar(automovil, energia, distancia, tiempo)
    # Calcula la distancia recorrida
    obtener_distancia = automovil.distancia(velocidad)
    distancia, tiempo = automovil.get_distancia()
    print("El Auto recorrio", distancia, "Km en", tiempo, "segundos")
    # Energia gastada
    energia_gastada = automovil.gasto_bateria(energia, distancia)
    print("Gasto", energia_gastada, "Wh/kg")
    energia = energia - energia_gastada
    return energia


# Si el automovil se queda sin energia se cierra la ejecucion
def fin(energia):
    if energia <= 0:
        print("El auto se quedp sin bateria y se destruyo")
        sys.exit()


def panel_control(estado, energia, ruedas, tiempo, automovil):
    print("Panel de control auto electrico 2000")
    # Se imprimen las especificaciones del autp
    if estado == 0:
        print("Auto Apagado")
    elif estado == 1:
        print("Auto encendido")
    fin(energia)
    print("Carga: ", energia, "Wh/kg")
    print("Estado de ruedas")
    estado_ruedas(ruedas)
    # Menu de accion
    print("Controles")
    print("e.- Encender / Apagar")
    print("w.- si quiere avanzar")
    eleccion = input("Que desea hacer: ")
    if eleccion == "e" and estado == 0:
        estado = 1
        os.system("clear")
        panel_control(estado, energia, ruedas, tiempo, automovil)
    elif eleccion == "e" and estado == 1:
        estado = 0
        os.system("clear")
        panel_control(estado, energia, ruedas, tiempo, automovil)
    if eleccion == "w" and estado == 0:
        print("El auto esta apagado no puede avanzar")
        panel_control(estado, energia, ruedas, tiempo, automovil)
    elif eleccion == "w" and estado == 1:
        os.system("clear")
        pass
    if eleccion != "e" and eleccion != "w":
        os.system("clear")
        panel_control(estado, energia, ruedas, tiempo, automovil)
    return estado


if __name__ == "__main__":
    estado = 0
    tiempo = 0
    distancia = 0
    velocidad = 0
    automovil = Auto()
    ruedas = automovil.get_ruedas()
    energia = automovil.get_bateria()
    # Ciclo infinito para que corra el programa
    while True:
        estado = panel_control(estado, energia, ruedas, tiempo, automovil)
        energia = avanzar(automovil, energia, distancia, tiempo)
        ruedas = gasto_ruedas(ruedas)
