#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import random


class Auto():
    def __init__(self):
        # inician las caracteristicas
        self.__velocidad = 0
        self.__distancia = 0
        self.__tiempo = random.randint(1, 11)
        self.__ruedas = [100, 100, 100, 100]
        self.__bateria = 400

    def velocidad(self):
        # El usuario ingresa la velocidad
        self.__velocidad = int(input("Ingrese la velocidad del automovil (velocidad maxima 120 km/h): "))

    def get_velocidad(self):
        return self.__velocidad

    def distancia(self, velocidad):
        # Se calcula la distancia recorrida
        self.__tiempo = random.randint(1, 11)
        self.__distancia = self.__velocidad * self.__tiempo

    def gasto_bateria(self, energia, distancia):
        # Calcula energia gastada
        self.__energia_gastada = (distancia * 0.5)/20
        return self.__energia_gastada

    def get_distancia(self):
        return self.__distancia, self.__tiempo

    def get_ruedas(self):
        return self.__ruedas

    def get_bateria(self):
        return self.__bateria
